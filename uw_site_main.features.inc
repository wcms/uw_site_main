<?php

/**
 * @file
 * uw_site_main.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_site_main_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
