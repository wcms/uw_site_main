<?php

/**
 * @file
 * Code for the uWaterloo main site feature.
 */

include_once drupal_get_path('module', 'uw_site_main') . '/uw_site_main.features.inc';

/**
 * Implements hook_date_formats().
 */
function uw_site_main_date_formats() {
  return array(
    array(
      'type' => 'short',
      'format' => 'Y-m-d g:i a',
      'locales' => array(),
    ),
    array(
      'type' => 'medium',
      'format' => 'D, Y-m-d g:i a',
      'locales' => array(),
    ),
    array(
      'type' => 'long',
      'format' => 'l, F j, Y - g:i a',
      'locales' => array(),
    ),
  );
}

/**
 * Implements hook_permission().
 *
 * Create permission to only allow admins to edit the WCMS web service user.
 */
function uw_site_main_permission() {
  return array(
    'access config page' => array(
      'title' => t('Access admin configuration page'),
      'description' => t('Gives a user access to the admin structure, configuration, help and index pages.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu_alter().
 */
function uw_site_main_menu_alter(&$items) {

  // Add permission to acess admin/structure page.
  $items['admin/structure']['access callback'] = 'user_access';
  $items['admin/structure']['access arguments'] = array('access config page');

  // Add permission to acess admin/config page.
  $items['admin/config']['access callback'] = 'user_access';
  $items['admin/config']['access arguments'] = array('access config page');

  // Add permission to acess admin/help page.
  $items['admin/help']['access callback'] = 'user_access';
  $items['admin/help']['access arguments'] = array('access config page');

  // Add permission to acess admin/index page.
  $items['admin/index']['access callback'] = 'user_access';
  $items['admin/index']['access arguments'] = array('access config page');
}
